 <!DOCTYPE html>

<html lang="en">
<head>
    <title>Sheila - Rentals</title>
    <meta charset="utf-8">
    <meta name="viewport"  content="width-device-width, initial-scale=1">

    <!-- css styles -->
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <!-- google-font -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700|Source+Sans+Pro:400,700" rel="stylesheet">

    <!-- favicon -->
    <link rel="icon" type="image/ico" href="img/favicon.ico">
</head>
<body>

<!-- header -->
    <header class="header">
        <div class="header-overlay">

            <nav class="navbar navbar-expand-lg">
                <a class="navbar-brand" href="index.php"><img src="img/mercyikpe.png" alt="logo" width="60"></a>
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse ml-5" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link active text-white" href="index.php">Home<span class="sr-only">(current)</span></a>
                        <a class="nav-item nav-link text-white ml-5 mr-5" href="all-categories.html">All Categories</a>
                        <a class="nav-item nav-link text-white" href="contact.html">Contact</a>
                    </div>
                </div>

                <form class="form-inline">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-warning my-2 my-sm-0 search-btn" type="submit">Search</button>
                </form>
            </nav>


            <div class="container text-center header-col">

                <p class="head-text">Rent <span class="header-text">movies</span> here</p>
                <p class="header-text">Sheila rentals </p>
                <p class="head-text">select from our collection of movies</p>
                <p class="head-text">Latest, Trending, Most popular movies</p>
                
            </div>

            </div>
    </header>


    <div class="container my-5">
        <ul class="nav justify-content-center mb-4">
            <li class="nav-item carousal-tab mr-4">
            <a class="nav-link active carousal-row" href="#">Popular</a>
            </li>

            <li class="nav-item carousal-tab">
            <a class="nav-link carousal-row" href="#">Latest</a>
            </li>
        </ul>
    </div>


    <div class="container mb-5">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="row">
                    <div class="col-md-3">
                        <img class="d-block w-100" src="img/img1.webp" alt="First slide">
                    </div>
                    <div class="col-md-3">
                        <img class="d-block w-100" src="img/img6.jpeg" alt="First slide">
                    </div>
                    <div class="col-md-3">
                        <img class="d-block w-100" src="img/img4.jpeg" alt="First slide">
                    </div>
                    <div class="col-md-3">
                        <img class="d-block w-100" src="img/family-watch.jpeg" alt="First slide">
                    </div>
                </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                    <div class="col-md-3">
                        <img class="d-block w-100" src="img/img8.jpeg" alt="First slide">
                    </div>
                    <div class="col-md-3">
                        <img class="d-block w-100" src="img/page-bg.jpeg" alt="First slide">
                    </div>
                    <div class="col-md-3">
                        <img class="d-block w-100" src="img/img13.jpeg" alt="First slide">
                    </div>
                    <div class="col-md-3">
                        <img class="d-block w-100" src="img/img14.jpeg" alt="First slide">
                    </div>
                </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                    <div class="col-md-3">
                        <img class="d-block w-100" src="img/img15.jpeg" alt="First slide">
                    </div>
                    <div class="col-md-3">
                        <img class="d-block w-100" src="img/img16.jpeg" alt="First slide">
                    </div>
                    <div class="col-md-3">
                        <img class="d-block w-100" src="img/img17.jpeg" alt="First slide">
                    </div>
                    <div class="col-md-3">
                        <img class="d-block w-100" src="img/family-watch.jpeg" alt="First slide">
                    </div>
                </div>
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

        <div class="text-center mt-5">
            <button type="button" class="btn btn-lg see-more-btn rounded-0">See More Movies</button>
        </div>
        
    </div>

    <div class="container mt-5 mb-5">
        <div class="card">
          <div class="card-body mt-4">
            <div class="top-border mx-5"></div>
            <p class="top-rent mx-5">Top Rent</p>
            <div class="card-deck my-4 mx-4">
              <div class="card">
                <img class="card-img-top" src="img/img15.jpeg" alt="Card image cap">
              </div>
              <div class="card">
                <img class="card-img-top" src="img/img16.jpeg" alt="Card image cap">
              </div>
              <div class="card">
                <img class="card-img-top" src="img/img17.jpeg" alt="Card image cap">
              </div>
               <div class="card">
                <img class="card-img-top" src="img/img13.jpeg" alt="Card image cap">
              </div>
            </div>
          </div>
        </div>
    </div>

    <div class="container mb-5">
        <div class="top-border"></div>
        <p class="top-rent">All Categories</p>
        <div class="row">
            <div class="col-6">
                <div class="row">
                    <div class="col-6">
                        <img class="card-img-top" src="img/img5.jpeg" alt="Card image cap">
                        <img class="card-img-top mt-4" src="img/img6.jpeg" alt="Card image cap">
                    </div>

                    <div class="col-6">
                        <img class="card-img-top" src="img/img5.jpeg" alt="Card image cap">
                        <img class="card-img-top mt-4" src="img/img13.jpeg" alt="Card image cap">
                    </div>

                </div>
            </div>
            <div class="col-6">
                <img class="card-img-top" src="img/family-watch.jpeg" alt="Card image cap">
            </div>
        </div> 
        <div class="row">
            <div class="col-6">
                <img class="card-img-top mt-2" src="img/img11.jpeg" alt="Card image cap">
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-6">
                        <img class="card-img-top" src="img/img6.jpeg" alt="Card image cap">
                        <img class="card-img-top mt-4" src="img/img3.jpeg" alt="Card image cap">
                    </div>

                    <div class="col-6">
                        <img class="card-img-top" src="img/img6.jpeg" alt="Card image cap">
                        <img class="card-img-top mt-4" src="img/img3.jpeg" alt="Card image cap">
                    </div>
                </div>
            </div>       
        </div> 

        <div class="text-right mt-5">
            <button type="button" class="btn btn-lg see-more-btn rounded-0">Browse More Categories</button>
        </div>
    </div>   
        
           <!-- Footer -->
    <section class="" id="footer">
        <div class="container py-5">
            <div class="top-border"></div>
        <p class="top-rent text-white">More About Us</p>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
                    <ul class="list-unstyled list-inline social text-center">
                        <li class="list-inline-item"><a href="https://web.facebook.com/mercy.ikpe.79"><i class="fab fa-facebook fa-lg"></i></a></li>
                        <li class="list-inline-item"><a href="https://twitter.com/mercyikpee"><i class="fab fa-twitter fa-lg"></i></a></li>
                        <li class="list-inline-item"><a href="https://medium.com/@mercyikpe"><i class="fab fa-medium fa-lg"></i></a></li>
                    </ul>
                </div>
                </hr>
            </div>  
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                    <p class="footer-text">Sheila Rentals is where you rent all movies with recommendations on newest, popular, and all categories</p>
                    <p class="">&copy All right Reversed.<span class="text-warning ml-2">MercyIkpe</span></p>
                </div>
                </hr>
            </div>  
        </div>
    </section>
    <!-- ./Footer -->   


    <!-- bootstrap scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>       
</body>


</html>